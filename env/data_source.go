package env

import (
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
	"gorm.io/gorm/logger"
	"log"
)

var DB *gorm.DB

func init() {
	var environment Environment
	environment = ApplicationEnvironment{}

	dsn := environment.GetProperty("db.username") + ":" +
		environment.GetProperty("db.password") + "@tcp(" +
		environment.GetProperty("db.url") + ")/" +
		environment.GetProperty("db.name") + "?charset=utf8&parseTime=True&loc=Local"
	db1, err := gorm.Open(mysql.Open(dsn), &gorm.Config{Logger: logger.Default.LogMode(logger.Info)})
	if err != nil {
		log.Fatal("", err)
	} else {
		DB = db1
	}
}
