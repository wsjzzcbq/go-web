package env

type Environment interface {
	GetProperty(key string) string
}
