package env

import "github.com/spf13/viper"

func init() {
	viper.SetConfigFile("./application.properties")
	viper.ReadInConfig()
}

type ApplicationEnvironment struct {
}

func (ae ApplicationEnvironment) GetProperty(key string) string {
	value := viper.Get(key)
	if value != nil {
		return value.(string)
	}
	return ""
}
