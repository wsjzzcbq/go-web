# go-web

#### 介绍
Gin+Gorm+sessions 搭建 golang web项目

#### 软件架构
golang 版本1.18+  
数据库 mysql8  


#### 安装教程

1.  将项目导入编辑器后，直接运行 main.go  

2.  详细内容可以看本人博客：[Gin+Gorm+sessions 搭建 golang web项目](https://blog.csdn.net/wsjzzcbq/article/details/127268924)
