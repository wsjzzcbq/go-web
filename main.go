package main

import (
	"github.com/gin-contrib/sessions"
	"github.com/gin-contrib/sessions/cookie"
	"github.com/gin-gonic/gin"
	"go-web/controller"
	"go-web/filter"
)

func main() {
	e := gin.Default()

	//设置session
	store := cookie.NewStore([]byte("secret"))
	//session注入中间件
	e.Use(sessions.Sessions("mysession", store))

	//html页面位置
	e.LoadHTMLGlob("templates/*")
	//静态文件位置
	e.Static("/static", "./static")

	e.GET("/index", controller.Index)
	e.GET("/user/add", controller.UserAdd)

	//测试session相关
	e.GET("/session/add", controller.SessionAdd)
	e.GET("/session/get", controller.SessionGet)

	//添加filter过滤路由
	bookApi := e.Group("/book")
	bookApi.Use(filter.DoFilter)
	bookApi.GET("/get", controller.GetBook)

	e.GET("/login", controller.Login)
	e.POST("/dologin", controller.DoLogin)
	e.GET("/logout", controller.DoLogout)

	e.GET("/two/user/add", controller.TwoUserAdd)

	//会对下面路由进行过滤
	e.Use(filter.DoLoginFilter)
	e.GET("/shop", controller.ShopList)

	//修改端口号为80
	e.Run("0.0.0.0:80")
}
