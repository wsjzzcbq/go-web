package dao

import (
	"go-web/entity"
	"go-web/env"
	"gorm.io/gorm"
)

func AddUser(user entity.User) {
	env.DB.AutoMigrate(&entity.User{})
	env.DB.Table("user").Save(&user)
}

func UpdateUser(DB *gorm.DB, user entity.User) error {
	return DB.Table("user").Model(&entity.User{}).
		Where("id = ?", user.Id).
		Update("name", user.Name).
		Update("age", user.Age).Error
}
