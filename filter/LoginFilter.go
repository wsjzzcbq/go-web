package filter

import (
	"fmt"
	"github.com/gin-contrib/sessions"
	"github.com/gin-gonic/gin"
)

func DoLoginFilter(c *gin.Context) {
	fmt.Println("登录过滤")
	fmt.Println(c.Request.URL)
	session := sessions.Default(c)
	if session.Get("user") == nil {
		c.Redirect(302, "/login")
	}
	c.Next()
}
