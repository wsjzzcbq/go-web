package controller

import (
	"github.com/gin-gonic/gin"
	"go-web/entity"
	"go-web/service"
	"strconv"
)

func UserAdd(c *gin.Context) {
	name := c.Query("name")
	age := c.Query("age")
	agei, _ := strconv.Atoi(age)
	service.UserAdd(name, agei)
	c.JSON(200, "添加成功")
}

func TwoUserAdd(c *gin.Context) {
	var user1 = entity.User{
		Id:   6,
		Name: "韦一笑",
		Age:  42,
	}
	var user2 = entity.User{
		Id:   8,
		Name: "杨逍",
		Age:  12,
	}
	var users []entity.User
	users = append(users, user1)
	users = append(users, user2)

	er := service.UpdateTwoUser(users)

	if er != nil {
		c.JSON(200, "修改失败")
	} else {
		c.JSON(200, "修改成功")
	}
}
