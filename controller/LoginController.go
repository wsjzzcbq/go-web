package controller

import (
	"fmt"
	"github.com/gin-contrib/sessions"
	"github.com/gin-gonic/gin"
)

func Login(c *gin.Context) {
	c.HTML(200, "login.html", nil)
}

func DoLogin(c *gin.Context) {
	username := c.PostForm("username")
	password := c.PostForm("password")

	fmt.Println(username)
	fmt.Println(password)

	session := sessions.Default(c)
	session.Set("user", "userinfo")
	session.Save()
	c.JSON(200, "登录成功")
}

func DoLogout(c *gin.Context) {
	session := sessions.Default(c)
	if session.Get("user") != nil {
		session.Delete("user")
		session.Save()
	}
	c.JSON(200, "登出成功")
}
