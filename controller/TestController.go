package controller

import (
	"github.com/gin-contrib/sessions"
	"github.com/gin-gonic/gin"
)

// SessionAdd 添加name到session
func SessionAdd(c *gin.Context) {
	session := sessions.Default(c)
	if session.Get("name") == nil {
		session.Set("name", "ShakeSpeare")
		//每次操作完session后必须调用Save方法，否则不生效
		session.Save()
	}
	c.JSON(200, "添加完成")
}

// SessionGet 从session中获取name
func SessionGet(c *gin.Context) {
	session := sessions.Default(c)
	name := session.Get("name")
	c.JSON(200, name)
}
